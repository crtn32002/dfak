---
name: Digital Defenders Partnership
website: https://www.digitaldefenders.org
logo: DDP_logo_zwart_RGB.png
languages: Español, English, Français, Português, Deutsch, Nederlands, Русский
services: grants_funding, in_person_training, org_security, equipment_replacement, assessment, secure_comms, device_security, vulnerabilities_malware
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso
hours: Monday-Thursday 9am-5pm CET
response_time: 4 أيّام
contact_methods: email, phone, mail
email: team@digitaldefenders.org
mail: Raamweg 16, 2596 HL Den Haag
phone: ‭+31 070 376 5500‬
---

شراكة المدافعين الرقميّين DDP تهدف إلى حماية حرِّيَّة الإنترنت و&nbsp;تعضيدها، و&nbsp;إلى وقاية الإنترنت مِنْ المخاطر الناشئة، بخاصة في البيئات القمعيّة. و&nbsp;هي تُنسِّق الإغاثة الطارئة للأفراد و&nbsp;المنظّمات، من المدافعين عن حقوق الإنسان، و&nbsp;الصحافيّين و&nbsp;نشطاء المجتمع المدني و&nbsp;المدوّنين. و&nbsp;المنظّمة تتَّبع مقاربة أساسُها الناس بالتركيز على قيَم الشفافية، و&nbsp;حقوق الإنسان، و&nbsp;الاستيعاب و&nbsp;التنوّع، و&nbsp;المساواة، و&nbsp;السريّة و&nbsp;الحريّة. تأسّست DDP سنة 2012 بواسطة ائتلاف الحريَّة على الخطّ FOC.

لدى DDP ثلاثة فئات مِنْ التمويل في الحالات الطارئة، و&nbsp;كذلك منحات أطول مدىً تُركِّز على بناء القدرات في المنظّمات. علاوة على ذلك فإنّها تنظِّم زمالة السلامة الرقميَّة التي تتلقّى المنظُّمات بموجبها تدريبات في الأمان الرقمي و&nbsp;الخصوصية مفصّلة لها، كما تنظّم برنامج شبكة الاستجابة السريعة.

