---
layout: page
title: واتسأپ
author: mfc
language: ar
summary: وسائل الاتصال
date: 2018-09
permalink: /ar/contact-methods/whatsapp.md
parent: /ar/
published: true
---

استعمال واتسأپ يحمي سرِّيَّة فحوى الاتّصال فلا يمكن لغير المتراسلين الاطّلاع على فحواها، لكنَّ واقعة الاتّصال قد تكون معلومة للحكومة أو لجهات إنفاذ القانون.

موارد: [How to: Use WhatsApp on Android](https://ssd.eff.org/en/module/how-use-whatsapp-android) [How to: Use WhatsApp on iOS](https://ssd.eff.org/en/module/how-use-whatsapp-ios).
