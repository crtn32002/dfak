---
layout: page
title: Tor
author: mfc, Metamorphosis Foundation
language: sq
summary: Metodat e kontaktimit
date: 2018-09
permalink: /sq/contact-methods/tor.md
parent: /sq/
published: true
---

Shfletuesi Tor është një shfletues interneti i përqendruar në privatësi që ju mundëson të lidheni me ueb-faqet në mënyrë anonime duke mos e shpërndarë vendndodhjen tuaj (përmes adresës suaj të IP) kur të qaseni një ueb-faqe.

Burime: [Overview of Tor](https://www.torproject.org/about/overview.html.en).
