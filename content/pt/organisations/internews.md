---
name: Internews
website: https://www.internews.org
logo: internews.png
languages: English, Español, Русский, العربية, Tagalog
services: in_person_training, org_security, ddos, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, account, forensic, censorship
beneficiaries: jornalistas, ddhs, ativistas, lgbti, mulheres, jovens, tsd
hours: 24/7, global
response_time: 12 horas
contact_methods: email, pgp
email: help@openinternetproject.org
pgp_key_fingerprint: 4439 FA33 F79C 2D4A 4CC8 9A4A 2FF2 08B9 BE64 58D0
initial_intake: yes
---

Complementar à sua atuação principal, o Internews atua também com indivíduos, organizações e comunidades ao redor do mundo para aumentar a consciência sobre a segurança digital, acesso protegido a uma internet aberta e sem censura, e melhorar as práticas de cuidado digital. O Internews tem treinado milhares de jornalistas e defensores de direitos humanos em mais de 80 países, e mantém fortes redes de treinadores de segurança digital e auditores familiares locais e regionais com o framework SAFETAG (https://safetag.org), um modelo de avaliação e plataforma de auditoria de segurança para grupos de advocacy, cujo o Internews conduziu o desenvolvimento. Construíndo parcerias responsivas e fortes com ambas sociedade civil e empresas de inteligência e análise de ameaças do setor privado, e com capacidade de suportar parceiros diretamente mantendo presença online segura e sem censura, o Internews oferece intervenções técnicas e não-técnicas desde recursos de segurança infraestruturais através do framework SAFETAG até estratégias especializadas de mitigação baseadas em pesquisa de ameaças. Apoiamos diretamente análises de phising e malware para grupos de mídia e direitos humanos que sofrem ataques digitais direcionados.
