---
name: Greenhost
website: https://greenhost.net
logo: greenhost.jpg
languages: English, Deutsch, Nederlands
services: org_security, web_hosting, web_protection, assessment, secure_comms, vulnerabilities_malware, browsing, ddos
beneficiaries: ativistas, lgbti, mulheres, jovens, tsd
hours: 24/7, UTC+2
response_time: 4 horas
contact_methods: web_form, email, pgp, mail, phone
web_form: https://greenhost.net/about-us/contact/
email: support@greenhost.nl
pgp_key_fingerprint: 37CD 8929 D4F8 82B0 8F66 18C3 0473 77B4 B864 2066
phone: +31 20 489 04 44
mail: Johan van Hasseltkade 202, Amsterdam Noord, 1032 LP Amsterdam, Netherlands
initial_intake: yes
---

O Greenhost oferece serviços de TI com uma visão sustentável e ética. Nossas ofertas de serviço incluem hospedagem de sites, serviços em nuvem, e opções poderosas no nicho de segurança de informação. Colaborando com organizações estruturadas e parcerias técnicas pioneiras, nos esforçamos para oferecer oportunidades completas de acesso à internet a assinantes enquanto protegemos suas privacidades. Estamos ativamente envolvidos em desenvolvimento de código aberto, tomando parte em vários projetos nas áreas de tecnologia, jornalismo, cultura, educação, sustentabilidade e liberdade de Internet.
