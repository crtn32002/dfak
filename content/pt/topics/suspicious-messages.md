---
layout: page
title: "Recebi uma mensagem suspeita"
author: Abir Ghattas, Donncha Ó Cearbhaill, Claudio Guarnieri, Michael Carbone
language: pt
summary: "Recebi uma mensagem, link, ou email suspeito, o que devo fazer a respeito?"
date: 2019-03-12
permalink: /pt/topics/suspicious-messages
parent: /pt/
---

# Recebi uma mensagem suspeita

Você pode receber mensagens _suspeitas_ na sua caixa de email, contas de mídia social, ou aplicativos de mensagem. A forma mais comum de email suspeito é a tentativa de golpe digital, ou phishing. Emails de phishing tentam te fazer entregar informações pessoais, financeiras ou de acesso, tentando se passar por um endereço legítimo para induzir uma visita a um site, ou ligação a uma central de atendimento que, na verdade, são falsos. Estas mensagens também podem conter imagens ou arquivos infectados com programas maliciosos que serão instalados ao abrí-los.

Se você não tem certeza sobre a autenticidade de uma mensagem que recebeu, ou sobre o que fazer a respeito, você pode usar o seguinte questionário como um guia para diagnosticar a situação ou compartilhar a mensagem com organizações de confiança que poderão prover uma análise mais detalhada da sua mensagem.

Tenha em mente que receber uma mensagem suspeita não significa necessariamente que sua conta foi comprometida. Se você acredita que seu email ou mensagem é suspeita, não abra. Não responda o email, não clique em nenhum link, e nem baixe quaisquer arquivos anexados.

## Workflow

### intro

Você fez alguma destas coisas ao receber a mensagem ou link?

- [Eu cliquei no link](#link-clicked)
- [Eu coloquei minha senha](#account-security_end)
- [Eu baixei um arquivo](#device-security_end)
- [Eu respondi o email](#reply-personal-info)
- [Eu ainda não fiz nada](#do-you-know-sender)

### do-you-know-sender

Você reconhece a pessoa ou organização que enviou a mensagem? Tenha em vista que a mensagem pode ter sido [fraudada](https://en.wikipedia.org/wiki/Email_spoofing) para parecer alguém que você confia.

 - [É uma pessoa ou organização que eu confio](#known-sender)
 - [É um provedor de serviço (serviço de email, hospedagem, mídia social, banco etc)](#service-provider)
 - [A mensagem foi enviada por uma pessoa ou organização desconhecida](#share)

### known-sender

> Você consegue entrar em contato com a pessoa ou organização usando outro canal de comunicação? Por exemplo, se você recebeu um email, pode verificar diretamente por telefone ou WhatsApp? Tenha certeza de usar um meio de contato já confirmado. Não dá para confiar em um número de telefone qualquer na assinatura de uma mensagem que já é considerada suspeita.

Você confirmou a veracidade do remetente?

 - [Sim](#resolved_end)
 - [Não](#share)

### service-provider

> Neste cenário, um provedor de serviço é uma empresa que provê algum tipo de serviço que você utilize ou possua inscrição. Esta lista pode conter seus serviços de email (Google, Yahoo, Microsoft, Protonmail etc), suas plataformas de mídia social (Facebook, Twitter, Instagram) ou plataformas online que tenham informações financeiras suas (Paypal, Amazon, Netflix, bancos etc).
>
> Existe alguma forma de verificar se a mensagem que recebeu é autêntica? Muitos destes provedores de serviço também fornecem cópias de notificações ou outros documentos na página de informações da sua conta. Por exemplo, se a mensagem for do Facebook, você deve ter uma cópia na sua [lista de emails de notificação](https://www.facebook.com/settings?tab=security&section=recent_emails), ou no caso do seu banco, você pode ligar para a central de atendimento.

Escolha uma das opções abaixo:

 - [Eu consegui verificar que a mensagem era legítima](#resolved_end)
 - [Eu não consegui verificar a mensagem](#share)
 - [Eu não tem cadastro neste serviço/Não tinha motivo para receber uma mensagem deste serviço](#share)

### link-clicked

> Em algumas mensagens suspeitas, os links podem tentar te levar para páginas de login falsas que irão roubar suas credenciais ou outros tipos de páginas que poderão roubar suas informações pessoais ou financeiras. O link pode em alguns casos pedir que você baixe algum anexo que tentará instalar programas maliciosos em seu dispositivo quando abertos.

Poderia nos dizer o que aconteceu após clicar no link?

 - [Ele me pediu para entrar com usuário e senha](#account-security_end)
 - [Ele baixou um arquivo](#device-security_end)
 - [Não parece ter acontecido nada mas não tenho certeza](#device-security_end)

### reply-personal-info

> Dependendo do tipo de informação compartilhada, você precisará tomar ações imediatas.

Que tipo de informação você compartilhou?

- [Eu compartilhei informações da minha conta](#account-security_end)
- [Eu compartilhei informações públicas](#share)
- [Não tenho certeza se compartilhei algo sensível e preciso de ajuda](#help_end)


### share

> Compartilhar sua mensagem suspeita pode ajudar a proteger pessoas próximas e sua comunidade que também pode estar sendo afetada.
>
> Para compartilhar sua mensagem suspeita, certifique-se de incluir o conteúdo da mensagem e também as informações sobre quem a enviou. Se a mensagem foi um email, por gentileza inclua o email inteiro incluindo o cabeçalho como mostra  [este guia](https://ajuda.locaweb.com.br/wiki/o-que-e-cabecalho-de-mensagem-e-como-obte-lo/).

Sente que irá precisar de apoio adicional?

- [Sim, preciso de mais apoio](#help_end)
- [Não, resolvi meu problema](#resolved_end)


### device-security_end

> Caso algum arquivo tenha sido baixado, a segurança de seu dispositivo pode estar em risco!

Por gentileza, contate uma das organizações abaixo que poderá auxiliar você nesta situação. Além disso, [compartilhe sua mensagem suspeita](#share).

:[](organisations?services=device_security)


### account-security_end

> Caso você tenha inserido suas credenciais, a segurança das suas contas pode estar em risco!
>
> Se você acredita que sua conta foi comprometida, recomendamos que siga o diagnóstico para [contas comprometidas](../../../account-access-issues) do Kit de Primeiros Socorros Digitais.
>
> Sugerimos que informe sua comunidade sobre a campanha de phishing, e compartilhe a mensagem suspeita com organizações que possam analisá-la.

Você gostaria de compartilhar informações sobre a mensagem que recebeu, ou precisa de assistência adicional?

- [Gostaria de compartilhar a mensagem suspeita](#share)
- [Preciso de ajuda para deixar minhas contas mais seguras](#account_end)
- [Preciso de ajuda para analisar a mensagem](#analysis_end)

### help_end

> Busque ajuda de colegas e pessoas nas suas redes e organizações para entender os riscos da exposição das informações compartilhadas. Além disso, é possível que estas outras pessoas também tenham recebido solicitações parecidas da mesma fonte ou de outras fontes suspeitas.

Por gentileza, contate uma das organizações abaixo que poderá auxiliar você nesta situação. Além disso, [compartilhe sua mensagem suspeita](#share).

:[](organisations?services=24_7_digital_support)


### account_end

> Se você está tendo suas contas comprometidas e precisa de ajuda para mantê-las seguras, por gentileza contate as organizações a seguir que poderão auxiliar com esta situação.

:[](organisations?services=account)


### analysis_end

As organizações a seguir poderão receber suas mensagens suspeitas e investigá-las mais a fundo:

:[](organisations?services=forensic&services=vulnerabilities_malware)

### resolved_end

Esperamos que o Kit tenha sido útil para você. Será um prazer receber suas considerações [através deste email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)


### final_tips

Primeira regra: nunca fornecer nenhuma informação pessoal por email. Instituições, bancos e serviços jamais pedirão estas informações via email.

Nem sempre é fácil dizer quando um email ou site é legítimo, mas existem algumas dicas que podem te ajudar a avaliar o email recebido.

* Senso de urgência: emails suspeitos geralmente alertam uma mudança repentina para uma conta e pedem que você aja imediatamente para verificá-la.
* Você verá no corpo do email pedidos para "confirmar" ou "atualizar sua conta" ou algo como "a falha na atualização resultará na suspensão de conta". É geralmente seguro assumir que nenhuma organização credível à qual você forneceu suas informações irá precisar que você os envie novamente, por isso não caia nesta armadilha.
* Cuidado com as mensagens não solicitadas, anexos, links e páginas de login.
* Cuidado com erros de ortografia e gramática.
* Clique para ver o endereço completo do remetente, não apenas o nome mostrado.
* Tenha cuidado com links encurtados - eles podem esconder um link malicioso atrás dele.
* Quando você passa o mouse por cima de um link, o endereço URL real para o qual você está sendo direcionado é mostrado em um popup ou embaixo de sua janela de navegador.



#### Resources

Seguem alguns recursos para identificar mensagens suspeitas e escapar do phishing.

* [Citizen Lab: Communities @ Risk report](https://targetedthreats.net) ​​​​​​​​​​​​​​
* [Security Self-Defense: Como evitar ataques de Pesca (Phishing)](https://ssd.eff.org/pt-br/module/como-evitar-ataques-de-pesca-phishing)
* [Security Security without borders: Guide to Phishing](https://guides.securitywithoutborders.org/guide-to-phishing/)​​​​​​​
