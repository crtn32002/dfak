---
layout: page
title: "Não consigo acessar minhas contas"
author: RaReNet
language: pt
summary: "Está tendo problemas ao acessar um email, mídia social ou a conta de um site? Sua conta mostra acessos que você não sabe de onde vêm? Existem várias coisas que você pode fazer para restringir estes problemas."
date: 2015-08
permalink: /pt/topics/account-access-issues/
parent: /pt/
---


# Não consigo acessar minhas contas

Contas de mídias sociais e aplicativos de chat e email são frequentemente utilizadas por pessoas da sociedade civil para comunicarem-se, trocarem conhecimentos e promover suas causas. Como consequência, estas contas são bastante visadas por malfeitores, que geralmente tentam prejudicar o acesso e as informações destas contas, causando prejuízos a estes membros da sociedade civil e seus contatos.

Este guia existe para ajudar no caso de alguma de suas contas ter sido comprometida.

Siga o questionário para identificar a natureza do problema e achar possíveis soluções.

## Workflow

### Password-Typo

> Às vezes não conseguimos entrar com a nossa senha no site porque podemos estar errando a digitação, ou o CapsLock foi ligado sem querer, ou mesmo quando estamos em outro dispositivo, a linguagem do teclado seja diferente daquele que usamos diariamente.
>
> Para verificar, tente abrir um editor de texto ou bloco de notas e digitar seu usuário e sua senha, confira, e cole no navegador ou aplicativo. Verifique também se o CapsLock está ligado (normalmente uma luz indica isto, mas nem todos os teclados possuem), ou se a linguagem do teclado está correta - indicada geralmente na barra do sistema, perto do relógio.

As sugestões acima ajudaram a entrar na sua conta?

- [Sim](#resolved_end)
- [Não](#What-Type-of-Account-or-Service)

### What-Type-of-Account-or-Service

Qual tipo de conta você está tentando acessar?

- [Facebook](#Facebook)
- [Página do Facebook](#Facebook-Page)
- [Twitter](#Twitter)
- [Google/Gmail](#Google)
- [Yahoo](#Yahoo)
- [Hotmail/Outlook/Live](#Hotmail)
- [Protonmail](#Protonmail)
- [Instagram](#Instagram)
<!--- - [AddOtherServiceLink](#service-Name) -->


### Facebook-Page

Sua página possui outras pessoas como admins?

- [Sim](#Other-admins-exist)
- [Não](#Facebook-Page-recovery-form)

### Other-admins-exist

Estas pessoas estão com o mesmo problema?

- [Sim](#Facebook-Page-recovery-form)
- [Não](#Other-admin-can-help)

### Other-admin-can-help

> Tente contatar estas pessoas para que tentem te adicionar como admin da página novamente.

Isso corrigiu o problema?

- [Sim](#Fb-Page_end)
- [Não](#account_end)


### Facebook-Page-recovery-form

> Por favor, entre no Facebook e use [este formulário de recuperação de páginas](https://www.facebook.com/help/contact/164405897002583)).
>
> Note que esta solicitação pode levar algum tempo até ser respondida. Salve este procedimento nos seus favoritos para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

O procedimento de recuperação funcionou para você?

- [Sim](#resolved_end)
- [Não](#account_end)


<!---=========================================================
//GoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogle
=========================================================-->

### Google

Você tem acesso ao email ou celular configurado para recuperar sua conta?

- [Sim](#I-have-access-to-recovery-email-google)
- [Não](#Recovery-Form-google)

### I-have-access-to-recovery-email-google

Confira seu e-mail de recuperação. Você recebeu um email do Google dizendo "Alerta de segurança para sua Conta do Google vinculada"?

- [Sim](#Email-received-google)
- [Não](#Recovery-Form-google)

### Email-received-google

Dentro do e-mail existe um link "Recuperar sua conta"?

- [Sim](#Recovery-Link-Found-google)
- [Não](#Recovery-Form-google)

### Recovery-Link-Found-google

> Utilize o link enviado neste email para iniciar o processo de recuperação da conta.

Você conseguiu recuperar sua conta?

- [Sim](#resolved_end)
- [Não](#Recovery-Form-google)

### Recovery-Form-google

> Por favor, tente preencher [este formulário para recuperar sua conta](https://support.google.com/accounts/answer/7682439?hl=en).
>
> Note que esta solicitação pode levar algum tempo até ser respondida. Salve este procedimento nos seus favoritos para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

O procedimento de recuperação funcionou para você?

- [Sim](#resolved_end)
- [Não](#account_end)


<!---=========================================================
//YahooYahooYahooYahooYahooYahooYahooYahooYahooYahooYahooYahoo
=========================================================-->

### Yahoo

Você tem acesso ao email ou celular configurado para recuperar sua conta?

- [Sim](#I-have-access-to-recovery-email-yahoo)
- [Não](#Recovery-Form-Yahoo)

### I-have-access-to-recovery-email-yahoo

Confira seu e-mail de recuperação. Você recebeu um email do Yahoo dizendo "Alterar a senha da sua conta Yahoo"?

- [Sim](#Email-received-yahoo)
- [Não](#Recovery-Form-Yahoo)

### Email-received-yahoo

Dentro do e-mail existe um link "Recupere sua conta aqui"?

- [Sim](#Recovery-Link-Found-Yahoo)
- [Não](#Recovery-Form-Yahoo)

### Recovery-Link-Found-Yahoo

> Utilize o link enviado neste email para iniciar o processo de recuperação da conta.

Você conseguiu recuperar sua conta?

- [Sim](#resolved_end)
- [Não](#Recovery-Form-Yahoo)

### Recovery-Form-Yahoo

> Por favor, tente seguir [estas instruções para recuperar sua conta](https://help.yahoo.com/kb/account/fix-problems-signing-yahoo-account-sln2051.html?impressions=true).
>
> Note que esta solicitação pode levar algum tempo até ser respondida. Salve este procedimento nos seus favoritos para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

O procedimento de recuperação funcionou para você?

- [Sim](#resolved_end)
- [Não](#account_end)


<!---=========================================================
TwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitter
//========================================================= -->

### Twitter

Você tem acesso ao email ou celular configurado para recuperar sua conta?

- [Sim](#I-have-access-to-recovery-email-Twitter)
- [Não](#Recovery-Form-Twitter)

### I-have-access-to-recovery-email-Twitter

Confira seu e-mail de recuperação. Você recebeu um email do Twitter dizendo "Sua senha do Twitter foi alterada"?

- [Sim](#Email-received-Twitter)
- [Não](#Recovery-Form-Twitter)

### Email-received-Twitter

Dentro do e-mail existe um link "Recupere sua conta"?

- [Sim](#Recovery-Link-Found-Twitter)
- [Não](#Recovery-Form-Twitter)

### Recovery-Link-Found-Twitter

> Utilize o link enviado neste email para iniciar o processo de recuperação da conta.

Você conseguiu recuperar sua conta?

- [Sim](#resolved_end)
- [Não](#Recovery-Form-Twitter)

### Recovery-Form-Twitter

>Por favor, tente seguir [este formulário para recuperar sua conta](https://support.google.com/accounts/answer/7682439?hl=en).
>
> Note que esta solicitação pode levar algum tempo até ser respondida. Salve este procedimento nos seus favoritos para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

O procedimento de recuperação funcionou para você?

- [Sim](#resolved_end)
- [Não](#account_end)


<!---=========================================================
//ProtonmailProtonmailProtonmailProtonmailProtonmailProtonmailProtonmail
//========================================================= -->

### Protonmail

> Por favor, tente seguir [este formulário para recuperar sua conta](https://protonmail.com/support/knowledge-base/reset-password/).
>
> Note que esta solicitação pode levar algum tempo até ser respondida. Salve este procedimento nos seus favoritos para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

O procedimento de recuperação funcionou para você?

- [Sim](#resolved_end)
- [Não](#account_end)

<!---==================================================================
//MicorsoftHotmailLiveOutlookMicorsoftHotmailLiveOutlookMicorsoftHotmailLiveOutlook
//================================================================== -->

### Hotmail

Você tem acesso ao email ou celular configurado para recuperar sua conta?

- [Sim](#I-have-access-to-recovery-email-Hotmail)
- [Não](#Recovery-Form-Hotmail)

### I-have-access-to-recovery-email-Hotmail

Confira seu e-mail de recuperação. Você recebeu um email da Microsoft dizendo "Alteração de senha da sua conta Microsoft"?

- [Sim](#Email-received-Hotmail)
- [Não](#Recovery-Form-Hotmail)

### Email-received-Hotmail

Dentro do e-mail existe um link "Recupere sua senha"?

- [Sim](#Recovery-Link-Found-Hotmail)
- [Não](#Recovery-Form-Hotmail)

### Recovery-Link-Found-Hotmail

> Please use the "Reset your password" link to recover your account.

Were you able to recover your account with "Reset your password" link?

- [Sim](#resolved_end)
- [Não](#Recovery-Form-Hotmail)

### Recovery-Form-Hotmail

> Por favor, tente seguir [este formulário para recuperar sua conta](https://account.live.com/acsr).
>
> Note que esta solicitação pode levar algum tempo até ser respondida. Salve este procedimento nos seus favoritos para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

O procedimento de recuperação funcionou para você?

- [Sim](#resolved_end)
- [Não](#account_end)


### Facebook

Você tem acesso ao email ou celular configurado para recuperar sua conta?

- [Sim](#I-have-access-to-recovery-email-Facebook)
- [Não](#Recovery-Form-Facebook)

### I-have-access-to-recovery-email-Facebook

Confira seu e-mail de recuperação. Você recebeu um email do Facebook dizendo "Alteração de senha do Facebook"?

- [Sim](#Email-received-Facebook)
- [Não](#Recovery-Form-Facebook)

### Email-received-Facebook

Dentro do e-mail existe uma mensagem que pergunta "Foi você?", com um link para tomar medidas de segurança?

- [Sim](#Recovery-Link-Found-Facebook)
- [Não](#Recovery-Form-Facebook)

### Recovery-Link-Found-Facebook

> Utilize o link enviado neste email para iniciar o processo de recuperação da conta.

Você conseguiu recuperar sua conta?

- [Sim](#resolved_end)
- [Não](#Recovery-Form-Facebook)

### Recovery-Form-Facebook

> Por favor, tente seguir [este formulário para recuperar sua conta](https://www.facebook.com/login/identify).
>
> Note que esta solicitação pode levar algum tempo até ser respondida. Salve este procedimento nos seus favoritos para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

O procedimento de recuperação funcionou para você?

- [Sim](#resolved_end)
- [Não](#account_end)

<!--- ==================================================================
InstagramInstagramInstagramInstagramInstagramInstagramInstagramInstagram
//================================================================== not yet tested-->

### Instagram

Você tem acesso ao email ou celular configurado para recuperar sua conta?

- [Sim](#I-have-access-to-recovery-email-Instagram)
- [Não](#Recovery-Form-Instagram)

### I-have-access-to-recovery-email-Instagram

Confira seu e-mail de recuperação. Você recebeu um email do Microsoft dizendo "Sua senha do Instagram foi alterada"?

- [Sim](#Email-received-Instagram)
- [Não](#Recovery-Form-Instagram)

### Email-received-Instagram

Dentro do e-mail existe um link de recuperação de senha?

- [Sim](#Recovery-Link-Found-Instagram)
- [Não](#Recovery-Form-Instagram)

### Recovery-Link-Found-Instagram

> Utilize o link enviado neste email para iniciar o processo de recuperação da conta.

Você conseguiu recuperar sua conta?

- [Sim](#resolved_end)
- [Não](#Recovery-Form-Instagram)

### Recovery-Form-Instagram

> Por favor, tente seguir [este formulário para recuperar sua conta](https://help.instagram.com/149494825257596?helpref=search&sr=1&query=hacked).
>
> Note que esta solicitação pode levar algum tempo até ser respondida. Salve este procedimento nos seus favoritos para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

O procedimento de recuperação funcionou para você?

- [Sim](#resolved_end)
- [Não](#account_end)


### Fb-Page_end

Estamos felizes que seu problema foi resolvido! Leia as recomendações a seguir para diminuir as possibilidades de perder acesso novamente no futuro:

- Certifique-se de que todas as pessoas que administram a página ativem a autenticação em dois fatores nas suas contas.
- Garanta que apenas pessoas da sua confiança e que estejam ativamente engajadas com a página recebam acesso de administradoras.

### account_end

Se os procedimentos sugeridos neste diagnóstico não foram capazes de ajudar você a recuperar a conta, procure uma das seguintes organizações capazes de auxiliar com os próximos passos:

:[](organisations?services=account)

### resolved_end

Esperamos que o Kit tenha sido útil para você. Será um prazer receber suas considerações [através deste email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

Leia as recomendações a seguir para diminuir as possibilidades de perder acesso novamente no futuro:

- Uma boa dica é ativar a autenticação em dois fatores (2FA) sempre que estiver disponível para as suas contas pessoais.
- Nunca use a mesma senha duas vezes. Troque assim que possível as senhas repetidas.
- Usar um programa gerenciador de senhas vai te ajudar a guardar e criar senhas diferentes e mais fortes.
- Tenha cuidado com as informações que você irá acessar em redes públicas (wifi aberto, LAN house etc), e sempre que possível utilize uma VPN confiável ou o navegador Tor nos seus dispositivos.

#### resources

- [Access Now Helpline Community Documentation: Recommendations on Team Password Managers](https://communitydocs.accessnow.org/295-Password_managers.html)
- [Security Self-Defense: Como se proteger nas redes sociais](https://ssd.eff.org/pt-br/module/como-se-proteger-nas-redes-sociais)
- [Security Self-Defense: Criando senhas fortes](https://ssd.eff.org/pt-br/module/criando-senhas-fortes)





<!--- Se quiser criar um novo fluxo é só colar e editar o exemplo abaixo, substituindo o service_name pelo nome real do serviço utilizado:
#### service-name

Você tem acesso ao email ou celular configurado para recuperar sua conta?

- [Sim](#I-have-access-to-recovery-service-name)
- [Não](#Recovery-Form-service-name)

### I-have-access-to-recovery-email-service-name

Confira seu e-mail de recuperação. Você recebeu um email [nome do serviço] dizendo "[assunto do e-mail]"?

- [Sim](#Email-received-service-name)
- [Não](#Recovery-Form-service-name

### Email-received-service-name

> Dentro do e-mail existe um link "recuperar sua senha"?


- [Sim](#Recovery-Link-Found-service-name)
- [Não](#Recovery-Form-service-name)

### Recovery-Link-Found-service-name

> Utilize o link enviado neste email para iniciar o processo de recuperação da conta.

Você conseguiu recuperar sua conta?

- [Sim](#resolved_end)
- [Não](#Recovery-Form-service-name)

### Recovery-Form-service-name

> Por favor, tente seguir [este formulário para recuperar sua conta](link para o formulário de recuperação de senha do serviço).
>
> Note que esta solicitação pode levar algum tempo até ser respondida. Salve este procedimento nos seus favoritos para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

O procedimento de recuperação funcionou para você?

- [Sim](#resolved_end)
- [Não](#account_end)

-->
