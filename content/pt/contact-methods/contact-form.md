---
layout: page
title: Formulário de Contato
author: mfc
language: pt
summary: Métodos de contato
date: 2018-09
permalink: /pt/contact-methods/contact-form.md
parent: /pt/
published: true
---

Um formulário de contato geralmente protegerá sua mensagem à organização destinatária de modo que você e a organização apenas possam saber do que se trata. No entanto, o fato de que você contatou a organização estará acessível aos governos, autoridades, e outros atores com os devidos aparatos técnicos. Se você deseja ter uma camada adicional de privacidade com relação ao fato de contatar esta organização, prefira usar o Tor como seu navegador de acesso ao site e ao formulário de contato.
