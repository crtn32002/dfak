---
layout: page
title: "No puedo acceder a mi cuenta"
author: RaReNet
language: es
summary: "¿Tienes problemas para acceder a un correo electrónico, a una red social o a una cuenta web? ¿Alguna cuenta muestra actividad que no reconoces? Hay muchas cosas que puedes hacer para enfrentar este problema."
date: 2019-08
permalink: /es/topics/account-access-issues/
parent: /es/
---


# No puedo acceder a mi cuenta

Las redes sociales y canales de comunicación son ampliamente utilizados por miembros de la sociedad civil  para comunicarse, compartir conocimientos y defender sus causas. Como consecuencia, dichas cuentas son fuertemente monitoreadas por actores maliciosos, que a menudo intentan comprometerlas, causando daños a los miembros de la sociedad civil y sus contactos.

Esta guía existe para ayudarte en caso de que hayas perdido el acceso a una de tus cuentas por haber sido comprometida.

Aquí dispones de un cuestionario para identificar la naturaleza de tu problema y encontrar posibles soluciones.

## Workflow

### Password-Typo

> Algunas veces, es posible que no puedas iniciar sesión en tu cuenta porque estás escribiendo la contraseña de forma errada, o porque la configuración de idioma del teclado no es la que usas habitualmente o
porque tienes activado el bloqueo de mayúsculas.
>
> Intenta escribir tu nombre de usuario y contraseña en un editor de texto y copialos del editor para pegarlos en el formulario de inicio de sesión. También asegúrate de que la configuración de idioma de tu teclado sea la correcta, o incluso si tienes activado el bloqueo de mayúsculas.

¿Las sugerencias anteriores te ayudaron a iniciar sesión en tu cuenta?

- [Sí](#resolved_end)
- [No](#What-Type-of-Account-or-Service)

### What-Type-of-Account-or-Service

¿A qué tipo de cuenta o servicio has perdido el acceso?

- [Facebook](#Facebook)
- [Página de Facebook](#Facebook-Page)
- [Twitter](#Twitter)
- [Google/Gmail](#Google)
- [Yahoo](#Yahoo)
- [Hotmail/Outlook/Live](#Hotmail)
- [ProtonMail](#Protonmail)
- [Instagram](#Instagram)
<!--- - [AddOtherServiceLink](#service-Name) -->


### Facebook-Page

¿La página tiene otros administradores?

- [Sí](#Other-admins-exist)
- [No](#Facebook-Page-recovery-form)

### Other-admins-exist

¿Los otros administradores tienen el mismo problema?

- [Sí](#Facebook-Page-recovery-form)
- [No](#Other-admin-can-help)

### Other-admin-can-help

> Por favor, pídele a los otros administradores que te agreguen nuevamente como administrador de la página.

¿Esto solucionó el problema?

- [Sí](#Fb-Page_end)
- [No](#account_end)


### Facebook-Page-recovery-form

> Inicia sesión en Facebook y utiliza el [formulario de Facebook para recuperar la página](https://www.facebook.com/help/contact/164405897002583)).
>
> Ten en cuenta que puede pasar un tiempo antes de recibir una respuesta a tus solicitudes. Guarda esta página en tus marcadores y vuelve a esta página en unos días.


¿Ha funcionado el procedimiento de recuperación?

- [Sí](#resolved_end)
- [No](#account_end)


<!---=========================================================
//GoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogle
=========================================================-->

### Google

¿Tienes acceso al correo electrónico / móvil de recuperación asociado?

- [Sí](#I-have-access-to-recovery-email-google)
- [No](#Recovery-Form-google)

### I-have-access-to-recovery-email-google

Comprueba si recibiste un correo electrónico de "Alerta de seguridad crítica para tu cuenta de Google vinculada" de Google. ¿Lo recibiste?

- [Sí](#Email-received-google)
- [No](#Recovery-Form-google)

### Email-received-google

Por favor, comprueba si hay un enlace "recuperar tu cuenta". ¿Está ahí?

- [Sí](#Recovery-Link-Found-google)
- [No](#Recovery-Form-google)

### Recovery-Link-Found-google

> Por favor, utiliza el enlace "recuperar tu cuenta" para recuperar tu cuenta.

¿Pudiste recuperar tu cuenta?

- [Sí](#resolved_end)
- [No](#Recovery-Form-google)

### Recovery-Form-google

> Intenta seguir las instrucciones en ["Cómo recuperar tu cuenta de Google o Gmail"](https://support.google.com/accounts/answer/7682439?hl=es).
>
> Ten en cuenta que puede pasar un tiempo antes de recibir una respuesta a tus solicitudes. Guarda esta página en tus marcadores y vuelve a esta página en unos días.

¿Ha funcionado el procedimiento de recuperación?

- [Sí](#resolved_end)
- [No](#account_end)


<!---=========================================================
//YahooYahooYahooYahooYahooYahooYahooYahooYahooYahooYahooYahoo
=========================================================-->

### Yahoo

¿Tienes acceso al correo electrónico / móvil de recuperación conectado?

- [Sí](#I-have-access-to-recovery-email-yahoo)
- [No](#Recovery-Form-Yahoo)

### I-have-access-to-recovery-email-yahoo

Comprueba si recibiste un correo electrónico de "Cambio de contraseña para tu cuenta de Yahoo" de Yahoo. ¿Lo recibiste?

- [Sí](#Email-received-yahoo)
- [No](#Recovery-Form-Yahoo)

### Email-received-yahoo

Por favor, Verifica si hay un enlace "Recupera tu cuenta aquí". ¿Está ahí?

- [Sí](#Recovery-Link-Found-Yahoo)
- [No](#Recovery-Form-Yahoo)

### Recovery-Link-Found-Yahoo

> Por favor, utiliza el enlace "recuperar tu cuenta" para recuperar tu cuenta.

¿Pudiste recuperar tu cuenta?

- [Sí](#resolved_end)
- [No](#Recovery-Form-Yahoo)

### Recovery-Form-Yahoo

> Sigue las instrucciones en ["Solucionar problemas al iniciar sesión en tu cuenta de Yahoo](https://es.ayuda.yahoo.com/kb/account/Solucionar-problemas-para-iniciar-sesi%C3%B3n-en-tu-cuenta-de-Yahoo-sln2051.html) para recuperar tu cuenta.
>
> Ten en cuenta que puede pasar un tiempo antes de recibir una respuesta a tus solicitudes. Guarda esta página en tus marcadores y vuelve a esta página en unos días.

¿Ha funcionado el procedimiento de recuperación?

- [Sí](#resolved_end)
- [No](#account_end)


<!---=========================================================
TwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitter
//========================================================= -->

### Twitter

¿Tienes acceso al correo electrónico / móvil de recuperación asociado?

- [Sí](#I-have-access-to-recovery-email-Twitter)
- [No](#Recovery-Form-Twitter)

### I-have-access-to-recovery-email-Twitter

Verifica si recibiste un correo electrónico de "Tu contraseña de Twitter ha sido cambiada" de Twitter. ¿Lo recibiste?

- [Sí](#Email-received-Twitter)
- [No](#Recovery-Form-Twitter)

### Email-received-Twitter

Por favor, Comprueba si el mensaje contiene un enlace "recuperar tu cuenta". ¿Está ahí?

- [Sí](#Recovery-Link-Found-Twitter)
- [No](#Recovery-Form-Twitter)

### Recovery-Link-Found-Twitter

> Utiliza el enlace "recuperar tu cuenta" para recuperar tu cuenta.

¿Pudiste recuperar tu cuenta?

- [Sí](#resolved_end)
- [No](#Recovery-Form-Twitter)

### Recovery-Form-Twitter

> Intenta seguir los pasos en ["Solicitar ayuda para restaurar tu cuenta"](https://help.twitter.com/forms/restore).
>
> Ten en cuenta que puede pasar un tiempo antes de recibir una respuesta a tus solicitudes. Guarda esta página en tus marcadores y vuelve a esta página en unos días.

¿Ha funcionado el procedimiento de recuperación?

- [Sí](#resolved_end)
- [No](#account_end)


<!---=========================================================
//ProtonmailProtonmailProtonmailProtonmailProtonmailProtonmailProtonmail
//========================================================= -->

### Protonmail

> Sigue [las instrucciones para restablecer tu contraseña (En Inglés)](https://protonmail.com/support/knowledge-base/reset-password/)
>
> Ten en cuenta que puede pasar un tiempo antes de recibir una respuesta a tus solicitudes. Guarda esta página en tus marcadores y vuelve a esta página en unos días.

¿Ha funcionado el procedimiento de recuperación?

- [Sí](#resolved_end)
- [No](#account_end)

<!---==================================================================
//MicorsoftHotmailLiveOutlookMicorsoftHotmailLiveOutlookMicorsoftHotmailLiveOutlook
//================================================================== -->

### Hotmail

¿Tienes acceso al correo electrónico / móvil de recuperación asociado?

- [Sí](#I-have-access-to-recovery-email-Hotmail)
- [No](#Recovery-Form-Hotmail)

### I-have-access-to-recovery-email-Hotmail

Comprueba si recibiste un correo electrónico de "cambio de contraseña de cuenta de Microsoft" de Hotmail. ¿Lo recibiste?

- [Sí](#Email-received-Hotmail)
- [No](#Recovery-Form-Hotmail)

### Email-received-Hotmail

Comprueba si el mensaje contiene un enlace "Restablecer tu contraseña". ¿Está ahí?

- [Sí](#Recovery-Link-Found-Hotmail)
- [No](#Recovery-Form-Hotmail)

### Recovery-Link-Found-Hotmail

> Utiliza el enlace "Restablecer tu contraseña" para recuperar tu cuenta.

¿Pudiste recuperar tu cuenta con el enlace "Restablecer tu contraseña"?

- [Sí](#resolved_end)
- [No](#Recovery-Form-Hotmail)

### Recovery-Form-Hotmail

> Prueba [el formulario "Recuperar tu
cuenta"](https://account.live.com/acsr).
>
> Ten en cuenta que puede pasar un tiempo antes de recibir una respuesta a tus solicitudes. Guarda esta página en tus marcadores y vuelve a esta página en unos días.

¿Ha funcionado el procedimiento de recuperación?

- [Sí](#resolved_end)
- [No](#account_end)


### Facebook

¿Tienes acceso al correo electrónico / móvil de recuperación asociado?

- [Sí](#I-have-access-to-recovery-email-Facebook)
- [No](#Recovery-Form-Facebook)

### I-have-access-to-recovery-email-Facebook

Comprueba si recibiste un correo electrónico de "cambio de contraseña de Facebook" de Facebook. ¿Lo recibiste?

- [Sí](#Email-received-Facebook)
- [No](#Recovery-Form-Facebook)

### Email-received-Facebook

¿El correo electrónico contiene un mensaje que dice "Si no hiciste esto, asegura tu cuenta" con un enlace?

- [Sí](#Recovery-Link-Found-Facebook)
- [No](#Recovery-Form-Facebook)

### Recovery-Link-Found-Facebook

> Utiliza el enlace "Recuperar tu cuenta aquí" en el mensaje para recuperar tu cuenta.

¿Pudiste recuperar tu cuenta haciendo clic en el enlace?

- [Sí](#resolved_end)
- [No](#Recovery-Form-Facebook)

### Recovery-Form-Facebook

> Intenta con [el formulario para recuperar tu cuenta](https://es-la.facebook.com/login/identify).
>
> Ten en cuenta que puede pasar un tiempo antes de recibir una respuesta a tus solicitudes. Guarda esta página en tus marcadores y vuelve a esta página en unos días.

¿Ha funcionado el procedimiento de recuperación?

- [Sí](#resolved_end)
- [No](#account_end)

<!--- ==================================================================
InstagramInstagramInstagramInstagramInstagramInstagramInstagramInstagram
//================================================================== not yet tested-->

### Instagram

¿Tienes acceso al correo electrónico / móvil de recuperación asociado?

- [Sí](#I-have-access-to-recovery-email-Instagram)
- [No](#Recovery-Form-Instagram)

### I-have-access-to-recovery-email-Instagram

Comprueba si recibiste un correo electrónico de "Tu contraseña de Instagram ha sido cambiada" de Instagram. ¿Lo recibiste?

- [Sí](#Email-received-Instagram)
- [No](#Recovery-Form-Instagram)

### Email-received-Instagram

Por favor, comprueba si hay un enlace de recuperación. ¿Está ahí?

- [Sí](#Recovery-Link-Found-Instagram)
- [No](#Recovery-Form-Instagram)

### Recovery-Link-Found-Instagram

> Por favor, utilice el enlace "Recuperar tu cuenta aquí" para recuperar tu cuenta.

¿Pudiste recuperar tu cuenta?

- [Sí](#resolved_end)
- [No](#Recovery-Form-Instagram)

### Recovery-Form-Instagram

> Intenta seguir las instrucciones en ["Creo que mi cuenta de Instagram ha sido pirateada"](https://help.instagram.com/149494825257596?helpref=search&sr=1&query=hacked) para recuperar tu cuenta.
>
> Ten en cuenta que puede pasar un tiempo antes de recibir una respuesta a tus solicitudes. Guarda esta página en tus marcadores y vuelve a esta página en unos días.

¿Ha funcionado el procedimiento de recuperación?

- [Sí](#resolved_end)
- [No](#account_end)


### Fb-Page_end

¡Es maravilloso que tu problema esté resuelto! Lee estas recomendaciones para ayudarte a minimizar la posibilidad de perder el acceso a tu página en el futuro:

- Activar la autenticación en dos factores (o 2FA) para todos los administradores de la página.
- Asignar roles de administrador solo a las personas en las que confías y que sean rápidas ante incidentes.

### account_end

Si los procedimientos sugeridos en este flujo de trabajo no te han ayudado a recuperar el acceso a tu cuenta, puedes comunicarte con las siguientes organizaciones para solicitar más ayuda:

:[](organisations?services=account)

### resolved_end

Con suerte, la guía de DFAK te ha sido útil. Por favor danos tu opinión [vía email](mailto: incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

Lee estas recomendaciones para ayudar a minimizar la posibilidad de perder el acceso a tus cuentas en el futuro.

- Siempre es un buen consejo activar la autenticación de dos factores (2FA) para todas las cuentas que lo permitan.
- Nunca uses la misma contraseña para más de una cuenta. Si lo haces, cámbialas lo antes posible.
- El uso de un administrador de contraseñas te ayudará a crear y recordar contraseñas únicas y seguras para todas tus cuentas.
- Ten cuidado al usar redes wifi públicas abiertas que no sean de confianza y, en cualquier caso conéctate a ellas a través de un res privada virtual (VPN) o Tor.

#### resources

- [[Documentación para la comunidad de la línea de ayuda en seguridad digital de Access Now: Recomendaciones para administradores de contraseñas para equipos - En Inglés](https://communitydocs.accessnow.org/295-Password_managers.html)
- [Surveillance Self-Defense: cómo protegerse en las redes sociales](https://ssd.eff.org/es/module/protegi%C3%A9ndose-en-las-redes-sociales)
- [Surveillance Self-Defense: Creación de contraseñas seguras mediante administradores de contraseñas](https://ssd.eff.org/es/module/creando-contrase%C3%B1as-seguras#0)
