---
layout: page
title: "J'ai perdu l'accès à mes comptes"
author: RaReNet
language: fr
summary: "Avez-vous des problèmes pour accéder à un compte mail, sur un réseau social ou un site web ? Est-ce qu'un compte présente une activité que vous ne reconnaissez pas ? Il y a plusieurs choses que vous pouvez faire pour résoudre le problème."
date: 2015-08
permalink: /fr/topics/account-access-issues/
parent: /fr/
---


# I lost access to my account

Les réseaux sociaux et outils de communication en ligne sont largement utilisés par les membres de la société civile pour communiquer, partager des connaissances et défendre leurs causes. En conséquence, les comptes de ces services en ligne peuvent être une cible importante pour des acteurs malveillants qui vont tenter de les compromettre, causant ainsi du tort aux membres de la société civile et à leurs contacts.

Ce guide est là pour vous aider dans le cas où vous avez perdu l'accès à l'un de vos comptes parce qu'il a été compromis.

Voici un questionnaire pour identifier la nature de votre problème et trouver de possibles solutions.


## Workflow

### Password-Typo

> Parfois, on n'arrive pas à se connecter à un compte à cause de fautes de frappe pour saisir le mot de passe, ou parce que l'agencement du clavier n'est pas réglé sur la langue que l'on utilise habituellement, ou encore parce que la touche majuscule est activée.
>
> Essayez d'écrire votre nom d'utilisateur et votre mot de passe dans un éditeur de texte et copiez-collez les dans le formulaire de connexion. Assurez-vous également que les paramètres de langue du clavier sont bons et vérifiez que la touche majuscule n'est pas activée.

Est-ce que les suggestions précédentes vous ont aidées pour vous connecter à votre compte ?

- [Oui](#resolved_end)
- [Non](#What-Type-of-Account-or-Service)


### What-Type-of-Account-or-Service

A quel type de compte ou de service en ligne avez-vous perdu l'accès ?

- [Facebook](#Facebook)
- [Facebook Page](#Page-Facebook)
- [Twitter](#Twitter)
- [Google/Gmail](#Google)
- [Yahoo](#Yahoo)
- [Hotmail/Outlook/Live](#Hotmail)
- [Protonmail](#Protonmail)
- [Instagram](#Instagram)
<!--- - [AddOtherServiceLink](#service-Name) -->


### Page-Facebook

Est-ce que la page a d'autres administrateurs ?

- [Oui](#Other-admins-exist)
- [Non](#Facebook-Page-recovery-form)


### Other-admins-exist

Est-ce que le(s) autre(s) administrateur(s) ont le même problème ?

- [Oui](#Facebook-Page-recovery-form)
- [Non](#Other-admin-can-help)


### Other-admin-can-help

> Demandez à un autre administrateur de vous ajouter à nouveau en tant qu'administrateur de la page.

Est-ce que cela a réglé le problème ?

- [Oui](#Fb-Page_end)
- [Non](#account_end)


### Facebook-Page-recovery-form

> Connectez-vous à Facebook et utilisez [ce formulaire pour récupérer la page](https://www.facebook.com/help/contact/164405897002583)).
>
> Veuillez noter que cela peut prendre un certain temps pour recevoir une réponse à votre requête. Sauvegardez cette page dans vos signets et revenez à ce déroulé dans quelques jours.

Est-ce que la procédure de récupération a marché ?

- [Oui](#resolved_end)
- [Non](#account_end)



<!---=========================================================
//GoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogle
=========================================================-->

### Google

Avez-vous accès à l'e-mail ou au téléphone mobile de récupération ?

- [Oui](#I-have-access-to-recovery-email-google)
- [Non](#Recovery-Form-google)


### I-have-access-to-recovery-email-google

Vérifiez si vous avez reçu un e-mail de Google "Alerte de sécurité critique concernant votre compte Google associé". L'avez-vous reçu ?

- [Oui](#Email-received-google)
- [Non](#Recovery-Form-google)


### Email-received-google

Veuillez vérifier s'il y a un lien "Récupérer son compte". Est-il là ?

- [Oui](#Recovery-Link-Found-google)
- [Non](#Recovery-Form-google)


### Recovery-Link-Found-google

> Veuillez utiliser le lien "Récupérer votre compte" pour récupérer votre compte.

Avez-vous pu récupérer votre compte ?

- [Oui]((#resolved_end)
- [Non](#Recovery-Form-google)

### Recovery-Form-google

> Veuillez essayer [ce formulaire de récupếration pour récupérer votre compte](https://support.google.com/accounts/answer/7682439?hl=fr).
>
> Veuillez noter que cela peut prendre un certain temps pour recevoir une réponse à votre requête. Sauvegardez cette page dans vos signets et revenez à ce déroulé dans quelques jours.

Est-ce que la procédure de récupération a marché ?

- [Oui](#resolved_end)
- [Non](#account_end)


<!---=========================================================
//YahooYahooYahooYahooYahooYahooYahooYahooYahooYahooYahooYahoo
=========================================================-->

### Yahoo

Avez-vous accès à l'e-mail ou au téléphone mobile de récupération ?

- [Oui](#I-have-access-to-recovery-email-yahoo)
- [Non](#Recovery-Form-Yahoo)


### I-have-access-to-recovery-email-yahoo

Veuillez vérifier si vous avez reçu un e-mail de Yahoo intitulé "Changez votre mot de passe pour votre compte Yahoo". L'avez-vous reçu ?

- [Oui](#Email-received-yahoo)
- [Non](#Recovery-Form-Yahoo)


### Email-received-yahoo

Veuillez vérifier s'il y a un lien "Récupérer son compte". Est-il là ?

- [Yes](#Recovery-Link-Found-Yahoo)
- [No](#Recovery-Form-Yahoo)


### Recovery-Link-Found-Yahoo

> Veuillez utiliser le lien "Récupérer votre compte" pour récupérer votre compte.

Avez-vous pu récupérer votre compte ?

- [Oui]((#resolved_end)
- [Non](#Recovery-Form-Yahoo)


### Recovery-Form-Yahoo

> Veuillez suivre [ces instructions pour récupérer votre compte](https://fr.aide.yahoo.com/kb/account/R%C3%A9soudre-les-probl%C3%A8mes-de-connexion-%C3%A0-votre-compte-Yahoo-sln2051.html?impressions=true).
>
> Veuillez noter que cela peut prendre un certain temps pour recevoir une réponse à votre requête. Sauvegardez cette page dans vos signets et revenez à ce déroulé dans quelques jours.

Est-ce que la procédure de récupération a marché ?

- [Oui](#resolved_end)
- [Non](#account_end)


<!---=========================================================
TwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitter
//========================================================= -->

### Twitter

Avez-vous accès à l'e-mail ou au téléphone mobile de récupération ?

- [Oui](#I-have-access-to-recovery-email-Twitter)
- [Non](#Recovery-Form-Twitter)


### I-have-access-to-recovery-email-Twitter

Veuillez vérifier si vous avez reçu un e-mail de Twitter intitulé "Votre mot de passe Twitter a été changé". L'avez-vous reçu ?

- [Oui](#Email-received-Twitter)
- [Non](#Recovery-Form-Twitter)


### Email-received-Twitter

Veuillez vérifier si le message contient un lien "Récupérer son compte". Est-il là ?

- [Oui](#Recovery-Link-Found-Twitter)
- [Non](#Recovery-Form-Twitter)


### Recovery-Link-Found-Twitter

> Veuillez utiliser le lien "Récupérer votre compte" pour récupérer votre compte.

Avez-vous pu récupérer votre compte ?

- [Oui]((#resolved_end)
- [Non](#Recovery-Form-Twitter)


### Recovery-Form-Twitter

> Veuillez essayer [ce formulaire de récupération pour récupérer votre compte](https://twitter.com/account/begin_password_reset).
>
> Veuillez noter que cela peut prendre un certain temps pour recevoir une réponse à votre requête. Sauvegardez cette page dans vos signets et revenez à ce déroulé dans quelques jours.

Est-ce que la procédure de récupération a marché ?

- [Oui](#resolved_end)
- [Non](#account_end)


<!---=========================================================
//ProtonmailProtonmailProtonmailProtonmailProtonmailProtonmailProtonmail
//========================================================= -->

### Protonmail

> Veuillez essayer [ce formulaire de récupération pour récupérer votre compte](https://protonmail.com/support/knowledge-base/reset-password/).
>
> Veuillez noter que cela peut prendre un certain temps pour recevoir une réponse à votre requête. Sauvegardez cette page dans vos signets et revenez à ce déroulé dans quelques jours.

Est-ce que la procédure de récupération a marché ?

- [Oui](#resolved_end)
- [Non](#account_end)


<!---==================================================================
//MicorsoftHotmailLiveOutlookMicorsoftHotmailLiveOutlookMicorsoftHotmailLiveOutlook
//================================================================== -->

### Hotmail

Avez-vous accès à l'e-mail ou au téléphone mobile de récupération ?

- [Oui](#I-have-access-to-recovery-email-Hotmail)
- [Non](#Recovery-Form-Hotmail)


### I-have-access-to-recovery-email-Hotmail

Veuillez vérifier si vous avez reçu un e-mail de Hotmail intitulé "Modifier le mot de passe de votre compte Microsoft". L'avez-vous reçu ?

- [Oui](#Email-received-Hotmail)
- [Non](#Recovery-Form-Hotmail)


### Email-received-Hotmail

Veuillez vérifier si le message contient un lien "Récupérer son compte". Est-il là ?

- [Oui](#Recovery-Link-Found-Hotmail)
- [Non](#Recovery-Form-Hotmail)


### Recovery-Link-Found-Hotmail

> Veuillez utiliser le lien "Réinitialiser votre mot de passe" pour récupérer votre compte.

Avez-vous pu récupérer votre compte avec ce lien ?

- [Oui]((#resolved_end)
- [Non](#Recovery-Form-Hotmail)


### Recovery-Form-Hotmail

> Veuillez essayer [ce formulaire de récupération pour récupérer votre compte](https://protonmail.com/support/knowledge-base/reset-password/).
>
> Veuillez noter que cela peut prendre un certain temps pour recevoir une réponse à votre requête. Sauvegardez cette page dans vos signets et revenez à ce déroulé dans quelques jours.

Est-ce que la procédure de récupération a marché ?

- [Oui](#resolved_end)
- [Non](#account_end)


### Facebook

Avez-vous accès à l'e-mail ou au téléphone mobile de récupération ?

- [Oui](#I-have-access-to-recovery-email-Facebook)
- [Non](#Recovery-Form-Facebook)


### I-have-access-to-recovery-email-Facebook

Veuillez vérifier si vous avez reçu un e-mail de Facebook intitulé "Modifier le mot de passe de votre compte Facebook". L'avez-vous reçu ?

- [Oui](#Email-received-Facebook)
- [Non](#Recovery-Form-Facebook)


### Email-received-Facebook

Est-ce que l'e-mail contient un message disant "Si vous n'avez pas fait cela, merci de sécuriser votre compte" avec un lien ?

- [Oui](#Recovery-Link-Found-Facebook)
- [Non](#Recovery-Form-Facebook)


### Recovery-Link-Found-Facebook

> Veuillez utiliser le lien "Récupérer votre compte" pour récupérer votre compte.

Avez-vous pu récupérer votre compte avec ce lien ?

- [Oui]((#resolved_end)
- [Non](#Recovery-Form-Facebook)


### Recovery-Form-Facebook

> Veuillez essayer [ce formulaire de récupération pour récupérer votre compte](https://www.facebook.com/login/identify).
>
> Veuillez noter que cela peut prendre un certain temps pour recevoir une réponse à votre requête. Sauvegardez cette page dans vos signets et revenez à ce déroulé dans quelques jours.

Est-ce que la procédure de récupération a marché ?

- [Oui](#resolved_end)
- [Non](#account_end)


<!--- ==================================================================
InstagramInstagramInstagramInstagramInstagramInstagramInstagramInstagram
//================================================================== not yet tested-->

### Instagram

Avez-vous accès à l'e-mail ou au téléphone mobile de récupération ?

- [Oui](#I-have-access-to-recovery-email-Instagram)
- [Non](#Recovery-Form-Instagram)


### I-have-access-to-recovery-email-Instagram

Veuillez vérifier si vous avez reçu un e-mail d'Instagram intitulé "Votre mot de passe Instagram a été changé". L'avez-vous reçu ?

- [Oui](#Email-received-Instagram)
- [Non](#Recovery-Form-Instagram)


### Email-received-Instagram

Veuillez vérifier si le message contient un lien de récupération de son compte. Est-il là ?

- [Oui](#Recovery-Link-Found-Instagram)
- [Non](#Recovery-Form-Instagram)


### Recovery-Link-Found-Instagram

> Veuillez utiliser le lien "Récupérer votre compte ici" pour récupérer votre compte.

Avez-vous pu récupérer votre compte avec ce lien ?

- [Oui]((#resolved_end)
- [Non](#Recovery-Form-Instagram)


### Recovery-Form-Instagram

> Veuillez essayer [ce formulaire de récupération pour récupérer votre compte](https://help.instagram.com/149494825257596?helpref=search&sr=1&query=hacked).
>
> Veuillez noter que cela peut prendre un certain temps pour recevoir une réponse à votre requête. Sauvegardez cette page dans vos signets et revenez à ce déroulé dans quelques jours.

Est-ce que la procédure de récupération a marché ?

- [Oui](#resolved_end)
- [Non](#account_end)


### Fb-Page_end

Nous sommes très contents que votre problème soit résolut. Veuillez lire ces recommandations pour vous aider à minimiser les risques de perdre l'accès à votre page dans le futur:

- Activez 2FA pour tous les administrateurs de la page
- N'attribuez le rôle d'administrateur qu'à des personnes de confiance qui peuvent être réactives


### account_end

Si les procédures suggérées dans ce déroulé ne vous ont pas aidé à récupérer l'accès à votre compte, vous pouvez contacter les organisations suivantes pour demander plus d'aide :

:[](organisations?services=account)

### resolved_end

Nous espérons que le guide DFAK a été utile. Merci de nous faire des retours [par e-mail](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)


### final_tips

Veuillez lire ces recommandations pour vous aider à minimiser les risques de perte d'accès à vos comptes dans le futur

- C'est toujours une bonne idée que d'activer l'authentification à deux facteurs (2FA) pour tous les comptes qui le proposent.
- Ne jamais utiliser le même mot de passe pour plus d'un compte. Si vous le faites, veuillez les changer dès que possible.
- Utiliser un gestionnaire de mots de passe vous aidera à créer et retenir des mots de passe solides et uniques pour tous vos comptes.
- Soyez précautionneux quand vous utilisez des réseaux wifi publics dans lesquels vous n'avez pas confiance, et si possible connectez-vous via un VPN ou Tor.

#### resources

- [Access Now Helpline Community Documentation: Recommendations on Team Password Managers](https://communitydocs.accessnow.org/295-Password_managers.html)
- [Security Self-Defense: Comment vous protéger sur les réseaux sociaux](https://ssd.eff.org/fr/module/comment-vous-prot%C3%A9ger-sur-les-r%C3%A9seaux-sociaux)
- [Security Self-Defense: Créer des mots de passe robustes en utilisant des gestionnaires de mots de passe](https://ssd.eff.org/fr/module/cr%C3%A9er-des-mots-de-passe-robustes#0)
