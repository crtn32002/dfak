---
layout: page.pug
title: "О проекте"
language: ru
summary: "О Комплекте экстренной цифровой помощи."
date: 2019-03-13
permalink: /ru/about/
parent: Home
---

"Комплект экстренной цифровой помощи" (The Digital First Aid Kit) — совместный проект [RaReNet (Rapid Response Network)](https://www.rarenet.org/) и [CiviCERT](https://www.civicert.org/).

Сеть быстрого реагирования (Rapid Response Network) — международное сообщество тех, кто оказывает оперативную помощь по вопросам цифровой безопасности. Среди участников сети — Access Now,  Amnesty Tech, Center for Digital Resilience, CIRCL, EFF, Freedom House, Front Line Defenders, Global Voices, Greenhost, Hivos & the Digital Defenders Partnership, Internews, La Labomedia, Open Technology Fund, Virtual Road. Сеть включает и отдельных экспертов, которые работают в сфере цифровой безопасности и оперативной поддержки.

Некоторые из этих организаций и частных лиц входят в CiviCERT. Это международная сеть помощников по цифровой безопасности и провайдеров инфраструктуры. CiviCERT сосредоточена главным образом на поддержке активистских групп и организаций, работающих ради социальной справедливости и защиты прав человека, в том числе цифровых. CiviCERT является профессиональным лейблом для разнообразных инициатив CERT (Компьютерной группы реагирования на чрезвычайные ситуации, Computer Emergency Response Team). CiviCERT аккредитована Trusted Introducer, Европейской сетью компьютерных групп реагирования на чрезвычайные ситуации.

Комплект экстренной цифровой помощи — [проект с открытым исходным кодом и принимает пожертвования на развитие](https://gitlab.com/rarenet/dfak).

Если вы хотите использовать Цифровую аптечку в условиях, когда подключение ограничено, или поиск подключения затруднен, [вы можете загрузить автономную версию здесь](https://www.digitalfirstaid.org/dfak-offline.zip).

Для любого комментария, предложения или вопроса по поводу Цифровой аптечки вы можете написать: dfak @ digitaldefenders . org

GPG - Отпечаток пальца: 1759 8496 25C1 56EC 1EB4 1F06 6CC1 888F 5D75 706B
