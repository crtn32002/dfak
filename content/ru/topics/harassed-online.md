---
layout: page
title: "Вы столкнулись с онлайновыми домогательствами?"
author: Floriana Pagano
language: ru
summary: "Вы столкнулись с онлайновыми домогательствами?"
date: 2019-04-01
permalink: /ru/topics/harassed-online/
parent: Home
---

# Вы столкнулись с онлайновыми домогательствами?

Интернет (в частности, социальные сети) немало помогает гражданским активистам и организации быть услышанными и распространять информацию. С другой стороны, в некоторых случаях за такое самовыражение можно и пострадать. В особенности это касается женщин, сообщества ЛГБТ+ и других меньшинств. Им бывает непросто добиться возможности равного и свободного высказывания своего мнения без риска столкнуться с дурным обращением и жестокостью.

Проблема может проявляться в разных формах. Злоумышленники часто рассчитывают на безнаказанность. Этот расчёт нередко опирается на слабость законодательства по защите жертв домогательств в интернете (во многих странах). Но чаще всего негодяяем везёт, потому что выработка стратегии защиты непроста: она требует креативности и зависит от типа злоупотребления.

Вот почему важно понимать характер проблемы и прикинуть, какие шаги необходимо предпринять.

В этом разделе Комплекта экстренной цифровой помощи мы познакомим вас с базовыми шагами по защите от онлайновых домогательств.

## Workflow

### physical-wellbeing

Вы опасаетесь угрозы жизни или благополучию?

- [Да](#physical-risk_end)
- [Нет](#no-physical-risk)

### no-physical-risk

Вы полагаете, что злоумышленник добрался (или пытается добраться) до какого-нибудь из ваших устройств?

 - [Да](#device-compromised)
 - [Нет](#account-compromised)

### device-compromised

> Смените пароль доступа к устройству. Пароль должен быть уникальным, длинным и сложным:
>
> - [Mac OS](https://support.apple.com/ru-ru/HT202860)
> - [Windows](https://support.microsoft.com/ru-ru/help/4490115/windows-change-or-reset-your-password)
> - [iOS - Apple ID](https://support.apple.com/ru-ru/HT201355)
> - [Android](https://support.google.com/accounts/answer/41078?co=GENIE.Platform%3DAndroid&hl=ru)

Вы сумели "удалить" злоумышленника со своего устройства?

 - [Да](#account-compromised)
 - [Нет](../../../device-acting-suspiciously)

### account-compromised

> Если кто-то получил доступ к вашему устройству, он мог добраться и до онлайновых аккаунтов, а значит, прочесть ваши приватные сообщения, узнать ваши контакты, увидеть публикации, фотографии или видео "не для посторонних".

Заметили ли вы исчезновение каких-либо публикаций или сообщений? Другую активность, которая позволяет думать, что аккаунт скомпрометирован? Не забудьте проверить папку отправленных сообщений.

 - [Да](../../../account-access-issues)
 - [Нет](#impersonation)

### impersonation

Кто-либо представляется вами?

- [Да](../../../impersonated)
- [Нет](#doxing)

### doxing

Кто-нибудь публиковал приватные данные (например, изображения) без вашего разрешения?

- [Да](#doxing-yes)
- [Нет](#hate-speech)

### doxing-yes

Где были опубликованы эти данные?

- [В социальной сети](#doxing-sn)
- [На другом сайте](#doxing-web)

### doxing-sn

> Если ваши приватные данные (в частности, фотография) были опубликованы в социальной сети, вы можете сообщить о нарушении стандартов сообщества в соответствии с тем, как это принято на сайте соцсети. Вот рекомендации для популярных сетей:
>
> - [Google](https://www.cybercivilrights.org/online-removal/#google)
> - [Facebook](https://www.cybercivilrights.org/online-removal/#facebook)
> - [Twitter](https://www.cybercivilrights.org/online-removal/#twitter)
> - [Tumblr](https://www.cybercivilrights.org/online-removal/#tumblr)
> - [Instagram](https://www.cybercivilrights.org/online-removal/#instagram)

Получилось ли удалить информацию?

 - [Да](#one-more-persons)
 - [Нет](#harassment_end)

### doxing-web

> Для удаления информации с сайта следуйте [этим инструкциям](https://withoutmyconsent.org/resources/take-down).

Получилось ли удалить информацию с сайта?

- [Да](#one-more-persons)
- [Нет](#harassment_end)

### hate-speech

Злоупотребление связано с расой, полом, религией?

- [Да](#one-more-persons)
- [Нет](#harassment_end)


### one-more-persons

Сколько всего злоумышленников?

- [Один](#one-person)
- [Двое и больше](#more-persons)

### one-person

Вы знаете этого человека?

- [Да](#known-harasser)
- [Нет](#block-harasser)


### known-harasser

> Если вы знаете злоумышленника, подумайте о том, чтобы сообщить о нём властям. В каждой стране свои законы по защите людей от онлайновых домогательств. Выясните, какие законы существуют у вас в стране.
>
> Если собираетесь подать на злоумышленника в суд, позаботьтесь о юридической поддержке.

Вы хотите подать на злоумышленника в суд?

 - [Да](#legal_end)
 - [Нет](#block-harasser)


### block-harasser

> Знаете вы злоумышленника или нет, добрый совет: заблокируйте его в социальных сетях — всюду, где можно.

> - [Facebook](https://www.facebook.com/help/290450221052800)
> - [Twitter](https://help.twitter.com/ru/using-twitter/blocking-and-unblocking-accounts)
> - [Google](https://support.google.com/accounts/answer/6388749?co=GENIE.Platform%3DDesktop&hl=ru)
> - [Tumblr](https://tumblr.zendesk.com/hc/ru-ru/articles/231877648-Blocking-users)
> - [Instagram](https://help.instagram.com/426700567389543)

Удалось заблокировать злоумышленника?

 - [Да](#resolved_end)
 - [Нет](#harassment_end)


### more-persons

> Если против вас действует двое или больше человек, возможно, вы стали мишенью целой кампании преследования. Следует продумать оптимальную стретегию для вашего случая.
>
> О том, какие вообще бывают стратегии, читайте [здесь](https://www.takebackthetech.net/be-safe/hate-speech-strategies)

Выбрали подходящую стратегию?

 - [Да](#resolved_end)
 - [Нет](#harassment_end)

### harassment_end

> Если вы по-прежнему подвергаетесь домогательствам и нуждаетесь в индивидуальном подходе, пожалуйста, свяжитесь с организацией, которая способна вам помочь.

:[](organisations?services=harassment)


### physical-risk_end

> Если существуют физические риски, пожалуйста, свяжитесь с организацией, которая способна вам помочь.

:[](organisations?services=physical_security)


### legal_end

> Если нужна юридическая поддержка, свяжитесь с организацией, которая способна вам помочь.

:[](organisations?services=legal)

### resolved_end

Надеемся, это руководство было полезным. Пожалуйста, потратьте немного времени на обратную связь ([email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)).

### final_tips

- **Документируйте.** Очень рекомендуем документировать атаки и любые инциденты, которые вы наблюдаете: делайте скриншоты, сохраняйте сообщения, которые получаете от преследователей, и так далее. Если получится, заведите журнал, в котором можно будет систематизировать все записи, даты, время, что и где случилось, ID пользователей, скриншоты и т.д. Такой журнал поможет определить шаблоны действий атакующих. Если у вас нет на это сил, попробуйте вспомнить, кто бы мог какое-то время фиксировать инциденты для вас. Этот человек может быть только из тех людей, кому вы глубоко доверяете, потому что вам придётся передать ему данные для входа в ваши персональные аккаунты.  После того, как вы получите назад контроль над аккаунтом, не забудьте сменить пароль.

    - О том, как документировать инциденты, рассказывается на [этой странице](https://www.techsafety.org/documentationtips/).

- **Включите двухфакторную аутентификацию** для всех своих аккаунтов. Она неплохо помогает, когда надо остановить злоумышленника, нацелившегося на ваши аккаунты. Если есть выбор, не используйте двухфакторную аутентификацию на основе SMS-сообщений. Выбирайте вариант с одноразовыми кодами, генерируемыми в приложении, или с физическим ключом.

    - Если не уверены, какое решение выбрать, обратите внимание на [эту инфографику](https://www.accessnow.org/cms/assets/uploads/2017/09/Choose-the-Best-MFA-for-you.png) и [эту статью](https://www.eff.org/deeplinks/2017/09/guide-common-types-two-factor-authentication-web).
    - Советы по настройке двухфакторной аутентификации для основных платформ можно найти [здесь](https://www.eff.org/deeplinks/2016/12/12-days-2fa-how-enable-two-factor-authentication-your-online-accounts).

- **Имейте представление о том, как вы выглядите в интернете.** Селф-доксинг заключается в изучении открытых источников информации о себе самом. Полезно, чтобы предотвратить ситуацию, когда злоумышленники воспользуются этой информацией и будут представляться вами.


#### Что почитать

- [Access Now Helpline Community Documentation: Guide to prevent doxing](https://guides.accessnow.org/self-doxing.html)
- [Access Now Helpline Community Documentation: FAQ - Online Harassment Targeting a Civil Society Member](https://communitydocs.accessnow.org/234-FAQ-Online_Harassment.html)​​​​​​​
- [Equality Labs: Anti-doxing Guide for Activists Facing Attacks from the Alt-Right](https://medium.com/@EqualityLabs/anti-doxing-guide-for-activists-facing-attacks-from-the-alt-right-ec6c290f543c)
- [FemTechNet: Locking Down Your Digital Identity](https://femtechnet.org/csov/lock-down-your-digital-identity/)
- [National Network to End Domestic Violence: Documentation Tips for Survivors of Technology Abuse & Stalking](https://www.techsafety.org/documentationtips)
